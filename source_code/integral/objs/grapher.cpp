#include "rt_graph.hpp"
#include "structures.hpp"
#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

namespace phi
{
    /**
     * @brief Construct a new axis::axis object
     * 
     * @param range range of value of this axis (Vector<float>::x min, Vector<float>::y max)
     * @param axes_color color of this axis
     */
    axis::axis(Vector2D<float> const &range, sf::Color const &axes_color) : range_{range}
    {
        if (range_.x() > range_.y())
        {
            throw std::logic_error("Invalid Axis Range");
        }
        if (!(numbers_font_.loadFromFile("resources/Roboto_Mono/RobotoMono-VariableFont_wght.ttf")))
        {
            throw std::logic_error("Could not load font from file, check all resource files are present");
        }
        axis_.setFillColor(axes_color);
        marker_.setFillColor(axes_color);
    }
    /**
     * @brief default constructor
     * 
     */
    Graph2D_Ft::Graph2D_Ft() : x_{Vector2D<float>{0, 1}, sf::Color{255, 255, 255}}, y_{Vector2D<float>{0, 1}, sf::Color{255, 255, 255}} {}
    /**
     * @brief Given the ranges constructs a empty graph, curves must be added through the add_curve method
     * 
     * @param x_range 2D vector representing min and max of x_ axis
     * @param y_range 2D vector representing min and max of y_ axis
     * @param graph_color Color of this graph (axes)
     */
    Graph2D_Ft::Graph2D_Ft(Vector2D<float> const &x_range, Vector2D<float> const &y_range, sf::Color const &graph_color) : x_{x_range, graph_color}, y_{y_range, graph_color} {}
    /**
     * @brief get this axis max value
     * 
     * @return float max value
     */
    float axis::get_max() const
    {
        return range_.y();
    }
    /**
     * @brief get this axis min value
     * 
     * @return float min value
     */
    float axis::get_min() const
    {
        return range_.x();
    }
    /**
     * @brief sets the range of this axis
     * 
     * @param range vector representing a range
     */
    void axis::set_range(const phi::Vector2D<float> &range)
    {
        if (range.x() > range.y())
        {
            throw std::logic_error("wrong axis range");
        }
        range_ = range;
    }
    /**
     * @brief set this axis max value
     * 
     * @param max max value 
     */
    void axis::set_max(float max)
    {
        if (range_.x() > max)
        {
            throw std::logic_error("wrong axis range");
        }
        range_(range_.x(), max);
    }
    /**
     * @brief set this axis min value
     * 
     * @param max min value 
     */
    void axis::set_min(float min)
    {
        if (min > range_.y())
        {
            throw std::logic_error("wrong axis range");
        }
        range_(min, range_.y());
    }
    /**
     * @brief set the axis title
     * 
     * @param title title string (Max 20 chars)
     */
    void axis::set_axis_title(std::string const &title)
    {
        if (title.size() < 20)
        {
            axis_title_ = title;
        }
    }
    /**
     * @brief draw this axis horizontally
     * 
     * @param target_window window to draw the axis on
     * @param size vector representing x and y sizes
     * @param position position vector
     * @param markers number of markers to draw
     */
    void axis::draw_horizontal_axis(sf::RenderWindow &target_window, Vector2D<float> const &size, Vector2D<float> const &position, int const markers)
    {
        if(markers < 0 )
        {
            throw std::logic_error("cannot draw negative number of markers");
        }
        float width = size.y() / 200.f;
        float interval = (range_.y() - range_.x()) / (markers);
        float value_range = (range_.y() - range_.x());
        float value = 0.f;
        sf::Text numbers;
        sf::Text title;

        numbers.setCharacterSize(50);
        numbers.setScale(1.f / 50.f * 5.f * width, 1.f / 50.f * 5.f * width);
        numbers.setFont(numbers_font_);
        numbers.setFillColor(axis_.getFillColor());

        title.setFont(numbers_font_);
        title.setString(axis_title_);
        title.setCharacterSize(50);
        title.setScale(1.f / 50.f * 5 * width, 1.f / 50.f * 5 * width);

        axis_.setSize(sf::Vector2f(size.x(), width));
        axis_.setPosition(sf::Vector2f(position.x(), position.y() + size.y()));
        target_window.draw(axis_);

        marker_.setSize(sf::Vector2f(width, 5 * width));
        marker_.setFillColor(sf::Color::White);

        while (value < value_range - 0.5f * interval)
        {
            value += interval;
            marker_.setPosition(sf::Vector2f(position.x() + (size.x() * value) / value_range, position.y() - 2 * width + size.y()));
            numbers.setPosition(sf::Vector2f(position.x() - 0.5f * numbers.getScale().x * float(numbers.getCharacterSize()) + (size.x() * value) / value_range, position.y() + 5 * width + size.y()));
            numbers.setString(std::to_string(int(value)));
            target_window.draw(numbers);
            target_window.draw(marker_);
        }
        title.setPosition(sf::Vector2f(position.x() + size.x() - title.getLocalBounds().width * title.getScale().x, position.y() + 5 * width + 1.5 * numbers.getLocalBounds().height * numbers.getScale().y + size.y()));
        target_window.draw(title);
    }
    /**
     * @brief draw this axis vertically
     * 
     * @param target_window window to draw the axis on
     * @param size vector representing x and y sizes
     * @param position position vector
     * @param markers number of markers to draw
     */
    void axis::draw_vertical_axis(sf::RenderWindow &target_window, Vector2D<float> const &size, Vector2D<float> const &position, int const markers)
    {
        if(markers < 0 )
        {
            throw std::logic_error("wrong axis range");
        }
        float width = size.y() / 200.f;
        float interval = (range_.y() - range_.x()) / (markers);
        float value_range = (range_.y() - range_.x());
        float value = 0.f;
        sf::Text numbers;
        sf::Text title;

        title.setFont(numbers_font_);
        title.setString(axis_title_);
        title.setCharacterSize(50);
        title.setScale(1.f / 50.f * 5 * width, 1.f / 50.f * 5 * width);
        title.setRotation(-90.f);

        numbers.setFont(numbers_font_);
        numbers.setCharacterSize(50);
        numbers.setScale(1.f / 50.f * 5 * width, 1.f / 50.f * 5 * width);
        numbers.setFillColor(axis_.getFillColor());

        axis_.setSize(sf::Vector2f(width, -size.y()));
        axis_.setPosition(sf::Vector2f(position.x(), position.y() + size.y()));
        target_window.draw(axis_);

        marker_.setSize(sf::Vector2f(5 * width, width));
        marker_.setFillColor(sf::Color::White);

        while (value < value_range - 0.5f * interval)
        {
            value += interval;
            std::string val = std::to_string(int(value));
            numbers.setString(val);
            marker_.setPosition(sf::Vector2f(position.x() - 2 * width, size.y() + position.y() - (size.y() * value) / value_range));
            numbers.setPosition(sf::Vector2f(position.x() - 5 * width - numbers.getLocalBounds().width * numbers.getScale().x, size.y() + position.y() - 0.25 * numbers.getCharacterSize() * numbers.getScale().x - (size.y() * value) / value_range));

            target_window.draw(numbers);
            target_window.draw(marker_);
        }
        title.setPosition(sf::Vector2f(position.x() - 5 * width - 2 * (numbers.getLocalBounds().width * numbers.getScale().x), position.y() + 4 * (title.getGlobalBounds().height * numbers.getScale().x)));
        target_window.draw(title);
    }
    /**
     * @brief set the X axis title
     * 
     * @param title title string (Max 20 chars)
     */
    void Graph2D_Ft::set_x_title(std::string const &title)
    {
        x_.set_axis_title(title);
    }
    /**
     * @brief set the Y axis title
     * 
     * @param title title string (Max 20 chars)
     */
    void Graph2D_Ft::set_y_title(std::string const &title)
    {
        y_.set_axis_title(title);
    }
    /**
     * @brief add a new curve to the graph
     * 
     * @param name string name of the curve
     * @param color color of the curve
     */
    void Graph2D_Ft::add_curve(std::string const &name, sf::Color const &color)
    {
        curve new_curve;
        new_curve.name = name;
        new_curve.curve_color = color;
        curves_.push_back(new_curve);
    }
    /**
     * @brief draw X and Y axes
     * 
     * @param target_window window to draw on
     */
    void Graph2D_Ft::draw_axes(sf::RenderWindow &target_window)
    {
        x_.draw_horizontal_axis(target_window, size_, position_, 5);
        y_.draw_vertical_axis(target_window, size_, position_, 10);
    }
    /**
     * @brief current state of the graph
     * 
     * @param target_window window to draw on
     * @param pos position of the graph on the window (X and Y)
     * @param size size of the graph (X and Y)
     */
    void Graph2D_Ft::draw_graph(sf::RenderWindow &target_window, Vector2D<float> const &pos, Vector2D<float> const &size)
    {
        position_ = pos;
        size_ = size;
        {
            int drawn_points = 0;
            sf::CircleShape c;
            c.setOutlineThickness(0);
            c.setRadius(size.y() / 250.f);
            for (auto const &curve : curves_)
            {
                c.setFillColor(curve.curve_color);
                for (auto const &point : curve.points)
                {
                    c.setPosition(pos.x() + (size.x() / (x_.get_max())) * point.x(), pos.y() + size.y() - point.y() * (size.y() / y_.get_max()));
                    target_window.draw(c);
                    ++drawn_points_;
                }
            }
            drawn_points_ = drawn_points;
        }
        draw_axes(target_window);
    }
    /**
     * @brief generate legend based on curves
     * 
     * @param target_window window to draw on
     */
    void Graph2D_Ft::draw_leg(sf::RenderWindow &target_window)
    {
        sf::RectangleShape leg_box;
        sf::Text entry;
        entry.setCharacterSize(25);
        entry.setScale(1.f / 50.f * 5.f * size_.y() * 0.01, 1.f / 50.f * 5.f * size_.y() * 0.01);
        entry.setFont(x_.numbers_font_);
        entry.setPosition(sf::Vector2f(position_.x() + 1.02 * size_.x(), position_.y()));
        leg_box.setOutlineThickness(0.005f * size_.x());
        leg_box.setPosition(sf::Vector2f(position_.x() + 1.01 * size_.x() + leg_box.getOutlineThickness(), entry.getPosition().y));
        leg_box.setFillColor(sf::Color::Transparent);

        for (auto const &curve : curves_)
        {
            entry.setFillColor(curve.curve_color);
            entry.setString(curve.name);
            target_window.draw(entry);
            entry.setPosition(sf::Vector2f(entry.getPosition().x, entry.getPosition().y + 1.25f * entry.getLocalBounds().height * entry.getScale().y));
        }
        leg_box.setSize(sf::Vector2f((entry.getLocalBounds().width + entry.getCharacterSize()) * entry.getScale().x, curves_.size() * 1.25f * entry.getLocalBounds().height * entry.getScale().y));
        leg_box.setOutlineColor(sf::Color::White);
        target_window.draw(leg_box);
    }
    /**
     * @brief add a new point to a curve
     * 
     * @param name string name of the curve
     * @param point vector of coordinates (x , F(x))
     * @return true point was added
     * @return false point was not added (curve not found)
     */
    bool Graph2D_Ft::add_point(std::string const &name, Vector2D<float> const &point)
    {
        int curve_index = 0;
        bool success = false;
        for (auto const &curve : curves_)
        {
            if (name == curve.name)
            {
                success = true;
                break;
            }
            ++curve_index;
        }
        if (success)
        {
            if (point.x() > x_.get_max())
            {
                x_.set_max(point.x());
            }
            if (point.y() > y_.get_max())
            {
                y_.set_max(point.y());
            }
            curves_[curve_index].points.push_back(point);
            ++total_points_;
            return true;
        }
        else
        {
            return false;
        }
    }
    int Graph2D_Ft::get_drawn_points() const
    {
        return drawn_points_;
    }
    int Graph2D_Ft::get_total_points() const
    {
        return total_points_;
    }
    Vector2D<float> Graph2D_Ft::get_Size() const
    {
        return size_;
    }
} // namespace sir