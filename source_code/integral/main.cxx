
#include "F1D.hxx"
#include "factorial.hxx"
#include "entity.hxx"
#include "structures.hpp"
#include "TF1.h"
#include <iostream>
#include <string>
#include <iomanip>

int main()
{
    
    //getting Diff parameters
    double x_low = 0.l;
    double x_up = 0.l;
    std::cout << "insert lower bound" << '\n';
    std::cin >> x_low;
    std::cout << "insert upper bound" << '\n';
    std::cin >> x_up;
    //double const normalization =  1 / sqrtl(2*M_PIl);
    FUNC1D<double> integrand([&](double x){ return 2*cos(x);} , 'x');
    std::cout << std::setprecision(50);
    std::cout << "Integrating: ";
    std::cout << '\n' << "From: " << x_low << " to " << x_up << '\n';
    auto start = std::chrono::high_resolution_clock::now();
    double integral1 = integrand.Integrate(x_low,x_up, int(1e3));
    auto stop = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    std::cout << "Integration Time 1:\t" << time.count() << "µs\n";
    std::cout << integral1 << '\n';
    

    TF1 Normal("","2*cos(x)");
    
    start = std::chrono::high_resolution_clock::now();
    //std::cout << integrand.Dn_Fx(0.l,1,sqrtl(std::numeric_limits<double>::epsilon())) << "\tD1" << '\n';
    double integral = Normal.Integral(x_low,x_up);
    stop = std::chrono::high_resolution_clock::now();
    time = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    std::cout << "Integration Time 2:\t" << time.count() << "µs\n" << integral << '\n';
/*
    std::cout << integrand.Dn_Fx(0.l,2,1e-3) << "\tD2" << '\n';
    std::cout << integrand.Dn_Fx(1.l,3,1e-3) << "\tD3" << '\n';


    double *values = new double[10];
    for(int n = 0; n != 10; ++n)
    {
        values[n] = n;
    }
    phi::RNVector<double, 10> v1(values);
    phi::RNVector<double, 10> v2(v1);
    phi::RNVector<double, 10> v3(v1 + v2);
    v1.PrintComponents();
    v2.PrintComponents();
    v3.PrintComponents();
  */  //std::cout << int_pow(2, 1);
    double result = 2*sinl(x_up) - 2*sinl(x_low);
    std::cout << "Error 1:\t" << result - integral1 << "\t Error 2:\t" << result - integral <<'\n';

    //Derivative testing
    FUNC1D<double> function([](double x){return exp(x);},'x');

    std::cout << "Testing n-th derivative precision ..." << '\n';
    double xx = 1.;
    int max_order = -1;
    std::cout << "Enter maximum order to compute, ";
    while(max_order < 0)
    {
        std::cout << "it must be a positive integer\n";
        std::cin >> max_order;
    }
    for (int i = 0; i != max_order; ++i)
    {
        auto Dn_Fx = function.Dn_Fx(xx,i); 
        std::cout << "f(" << i << ")(x):\n";
        std::cout << "value: " << Dn_Fx << '\t' << "error:" << Dn_Fx - exp(xx) << '\n'; 
    }
    
}
