/**
 * @file F1D.hxx
 * @author Federico Ciampiconi (foffy2001@duck.com)
 * @brief 
 * @version 0.1
 * @date 2025-03-07
 * 
 * @copyright Copyright (c) 2025 Federico Ciampiconi
 * Distributed under the MIT License (MIT)
 * see accompanying LICENSE.txt file
 * or a copy at https://opensource.org/licenses/MIT
 * 
 */

#ifndef FUNCT1D
#define FUNCT1D
#include "factorial.hxx"
#include <string>
#include <cmath>
#include <iostream>
#include <thread>
#include <atomic>
#include <type_traits>
#include <functional>
template <typename FP>
class FUNC1D
{
public:
    FUNC1D() = delete;
    FUNC1D(std::function<FP(const FP)> const &func, char const var);
    ~FUNC1D();
    FP Integrate(FP const x_low, FP const x_up, int const nSteps = 10) const;
    FP Fx(FP const x) const;
    FP Dn_Fx(FP const x, int const n, FP const eps = eps_) const;
    FP D2_Fx(FP const x, FP const eps = eps_) const;
    FP D4_Fx(FP const x, FP const eps = eps_) const;

private:
    static_assert(std::is_floating_point_v<FP>);
    char *PARS_;                                                                 //integration variable
    int sTerms_;                                                                 //number of terms in taylor expansion
    int nSteps_;                                                                 //
    static FP constexpr eps_ = 10 * sqrtl(sqrtl(std::numeric_limits<FP>::epsilon())); //sqrt(sqrt(representation machine epsilon))
    std::function<FP(const FP)> function_;
};

//CONSTRUCTORS
template <typename FP>
FUNC1D<FP>::FUNC1D(std::function<FP(const FP)> const &func, char const var) : function_{func}
{
    PARS_ = new char[1]{var};
    sTerms_ = 2;
}
template <typename FP>
FUNC1D<FP>::~FUNC1D()
{
    delete PARS_;
}

//CONST METHODS
/**
 * @brief Integrate this function object in an interval
 * 
 * @tparam FP       floating point representation of the function
 * @param x_low     lower bound
 * @param x_up      upper bound
 * @param nSteps    size integration steps
 * @return FP       integral numeric value
 */
template <typename FP>
inline FP FUNC1D<FP>::Integrate(FP const x_low, FP const x_up, int const nSteps) const
{
    //int const nSteps = 5e4; //abs((x_up - x_low) / dx);
    FP const dx = (x_up - x_low) / nSteps; //integration step
    //FP e = 0.;

    FP sum = FP{0}; //integral current value

    for (int step = 0; step != nSteps; ++step)
    {
        // Compute integral taylor expansion in current bin
        for (int term = 0; term != sTerms_ + 2; term += 2)
        {
            // Add expansion terms
            sum += Dn_Fx((x_low + step * dx) + (dx * FP{0.5l}), term) * int_pow(dx, term + 1) / (factorial(term + 1) * int_pow(2, term));
        }
        //e += Dn_Fx(x + (dx * 0.5l), sTerms_ , eps_) / (factorial(sTerms_ + 1) * int_pow(2, sTerms_)) * dx*dx*dx;
    }
    /*
    std::cout << (FP{x_low + dx * (nSteps-1)} != x_up) << '\t' << x_low + dx * nSteps << '\n';
    if (FP{x_low + (dx * nSteps)} != x_up)
        sum += Integrate(x_low + dx * (nSteps-1), x_up, 1);
    
    */
    return sum; 
}
/**
 * @brief Compute the n-th derivative of this function object
 * 
 * @tparam FP Floating point type
 * @param x   Derivative point
 * @param n   Order of Derivative
 * @param eps Epsilon size
 * @return FP Derivative value
 */
template <typename FP>
inline FP FUNC1D<FP>::Dn_Fx(FP const x, int const n, FP const eps) const
{
    if (n == 0)
        return Fx(x);
    /*else if (n == 2)
        return D2_Fx(x, eps);
    return D4_Fx(x, eps);*/

    return (8 * (Dn_Fx(x + eps, n - 1) - Dn_Fx(x - eps, n - 1)) - Dn_Fx(x + 2 * eps, n - 1) + Dn_Fx(x - 2 * eps, n - 1)) / (eps * 12);
}
template <typename FP>
FP FUNC1D<FP>::D2_Fx(FP const x, FP const eps) const
{
    return (Fx(x + 2 * eps) + Fx(x - 2 * eps) - 2 * Fx(x)) / (4 * eps * eps);
}
template <typename FP>
FP FUNC1D<FP>::D4_Fx(FP const x, FP const eps) const
{
    return (Fx(x + 4 * eps) + Fx(x - 4 * eps) - 2 * (Fx(x - 2 * eps) + Fx(x + 2 * eps)) + 6 * Fx(x)) / (16 * eps * eps * eps * eps);
}
template <typename FP>
FP FUNC1D<FP>::Fx(FP const x) const
{
    return function_(x);
}
#endif
