/**
 * @file entity.hxx
 * @author Federico Ciampiconi (foffy2001@duck.com)
 * @brief 
 * @version 0.1
 * @date 2025-03-07
 * 
 * @copyright Copyright (c) 2025 Federico Ciampiconi
 * Distributed under the MIT License (MIT)
 * see accompanying LICENSE.txt file
 * or a copy at https://opensource.org/licenses/MIT
 * 
 */

#ifndef ENTITY_HXX
#define ENTITY_HXX
#include <vector>
#include <string>
#include <type_traits>
#include "structures.hpp"
namespace phi
{
    template <typename T>
    class quantity
    {
    public:
        quantity(std::string const &um) : um_{um} {}
        virtual ~quantity() = 0;
        virtual int GetN() const;
        virtual phi::Vector2D<T> GetEstimate() const;
        virtual T GetEstimateVal() const;
        virtual T GetEstimateSig() const;
        virtual void Add_Values(std::vector<T> const &v, std::vector<T> const &w);
        virtual void Add_Value(phi::Vector2D<T> const &v);

    private:
        std::string um_;
        std::vector<T> values_;
        std::vector<T> weights_;
    };
    template <typename FP>
    class cQuantity : quantity<FP>
    {
    public:
        cQuantity(std::string const &um) : quantity<FP>::quantity(um) {}

    private:
        static_assert(std::is_floating_point_v<FP>);
        static constexpr FP eps_ = std::numeric_limits<FP>::epsilon();
    };
    template <typename Int>
    class dQuantity : quantity<Int>
    {
    public:
        dQuantity(std::string const &um) : quantity<Int>::quantity(um) {}

    private:
        static_assert(std::is_integral_v<Int>);
    };
    template <typename T>
    int quantity<T>::GetN() const
    {
        return values_.size();
    }
    template <typename T>
    phi::Vector2D<T> quantity<T>::GetEstimate() const
    {
        T sum = T{};
        T wsum = T{};
        for (int i = 0; i != values_.size(); ++i)
        {
            sum += weights_[i] * values_[i];
            wsum += weights_[i];
        }
        return phi::Vector2D<T>{sum / wsum, 1 / (sqrt(wsum))};
    }
    template <typename T>
    T quantity<T>::GetEstimateVal() const
    {
        T sum = T{};
        T wsum = T{};
        for (int i = 0; i != values_.size(); ++i)
        {
            sum += weights_[i] * values_[i];
            wsum += weights_[i];
        }
        return sum / wsum;
    }
    template <typename T>
    T quantity<T>::GetEstimateSig() const
    {
        T wsum = T{};
        for (auto const a : weights_)
        {
            wsum += a;
        }
        return 1 / sqrt(wsum);
    }
    /**
     * @brief add a weighted measurement
     * 
     * @tparam T scalar type
     * @param v 2D vector: (measurement, measurement weight)  weight = 1/sigma^2
     */
    template <typename T>
    void quantity<T>::Add_Value(phi::Vector2D<T> const &v)
    {
        values_.push_back(v.x());
        weights_.push_back(v.y());
        return;
    }
    /**
     * @brief add a vector of weighted measurements
     * 
     * @tparam T scalar type 
     * @param v measurement
     * @param w measurement weight (1 / sigma^2)
     */
    template <typename T>
    void quantity<T>::Add_Values(std::vector<T> const &v, std::vector<T> const &w)
    {
        values_ += v;
        weights_ += w;
        return;
    }
}; // namespace phi

#endif