/**
 * @file structures.hpp
 * @author Federico Ciampiconi (foffy2001@duck.com)
 * @brief 
 * @version 0.1
 * @date 2025-03-07
 * 
 * @copyright Copyright (c) 2025 Federico Ciampiconi
 * Distributed under the MIT License (MIT)
 * see accompanying LICENSE.txt file
 * or a copy at https://opensource.org/licenses/MIT
 * 
 */
#ifndef DATA_STRUCTURES_HPP
#define DATA_STRUCTURES_HPP
#include <vector>
#include <string>
#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <algorithm>

namespace phi
{
    /**
     * @brief class representing a 2-dimensional vector
     *
     */
    template <typename T>
    class Vector2D
    {
        //must be made of floating point numbers
        static_assert(std::is_floating_point_v<T>);
        T x_;
        T y_;

    public:
        Vector2D(T const x = T{}, T const y = T{})  : x_{x}, y_{y}
        {
        }

        //used to modify vector's coordinates
        inline void operator()(T const &new_x, T const &new_y)
        {
            x_ = static_cast<T>(new_x);
            y_ = static_cast<T>(new_y);
        }

        //used to read vector's coordinates
        inline T x() const
        {
            return x_;
        }
        inline T y() const
        {
            return y_;
        }
    };

    //useful operators
    template <typename T>
    inline bool operator==(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return lhs.x() == rhs.x() && lhs.y() == rhs.y();
    }

    template <typename T>
    inline bool operator!=(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return lhs.x() != rhs.x() || lhs.y() != rhs.y();
    }

    template <typename T>
    inline Vector2D<T> operator+(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return Vector2D{lhs.x() + rhs.x(), lhs.y() + rhs.y()};
    }

    template <typename T>
    inline Vector2D<T> operator-(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return Vector2D{lhs.x() - rhs.x(), lhs.y() - rhs.y()};
    }

    template <typename T>
    inline void operator+=(Vector2D<T> &lhs, Vector2D<T> const &rhs)
    {
        lhs = Vector2D{lhs.x() + rhs.x(), lhs.y() + rhs.y()};
    }

    template <typename T, typename F>
    inline Vector2D<T> operator*(F const &factor, Vector2D<T> const &vector)
    {
        return Vector2D{static_cast<T>(factor) * vector.x(), static_cast<T>(factor) * vector.y()};
    }

    template <typename T, typename F>
    inline Vector2D<T> operator*(Vector2D<T> const &vector, F const &factor)
    {
        return Vector2D{static_cast<T>(factor) * vector.x(), static_cast<T>(factor) * vector.y()};
    }

    //useful functions
    template <typename T>
    inline T norm(Vector2D<T> const &v)
    {
        return hypot(v.x(), v.y());
    }

    template <typename T>
    inline T dot_product(Vector2D<T> const &lhs, Vector2D<T> const &rhs)
    {
        return lhs.x() * rhs.x() + lhs.y() * rhs.y();
    }

    template <typename T, int const N>
    class RNVector
    {
    public:
        bool operator==(RNVector<T, N> const &v2) const;
        bool operator!=(RNVector<T, N> const &v2) const;
        T operator*(RNVector<T, N> const &v2) const;
        RNVector<T, N> operator+(RNVector<T, N> const &v2) const;
        RNVector<T, N> operator-(RNVector<T, N> const &v2) const;
        RNVector<T, N> operator*(T const s) const;

        RNVector() : vector_{new T[N]}
        {
            for (int n = 0; n != N; ++n)
            {
                vector_[n] = 0;
            }
        }
        RNVector(RNVector<T, N> const &v) : vector_{new T[N]}
        {
            for (int n = 0; n != N; ++n)
            {
                vector_[n] = v.vector_[n];
            }
        }
        RNVector(T *const v) : vector_{v} {}
        ~RNVector() { delete vector_; }
        T GetXn(int const n) const;
        int GetN() const;
        void PrintComponents() const;

    private:
        static_assert(std::is_scalar_v<T>);
        T *const vector_;
    };

    template <typename T, int const N>
    inline bool RNVector<T, N>::operator==(RNVector<T, N> const &v2) const
    {
        return std::equal(vector_, &vector_[N], v2.vector_, &v2.vector_[N]);
    }
    template <typename T, int const N>
    inline bool RNVector<T, N>::operator!=(RNVector<T, N> const &v2) const
    {
        return !std::equal(vector_, &vector_[N], v2.vector_, &v2.vector_[N]);
    }
    template <typename T, int const N>
    inline T RNVector<T, N>::operator*(RNVector<T, N> const &v2) const
    {
        T product = T{0};
        for (int n = 0; n != N; ++n)
        {
            product += vector_[n] * v2.vector_[n];
        }
        return product;
    }
    template <typename T, int const N>
    inline RNVector<T, N> RNVector<T, N>::operator+(RNVector<T, N> const &v2) const
    {
        T *const v = new T[N];
        for (int n = 0; n != N; ++n)
        {
            v[n] = vector_[n] + v2.vector_[n];
        }
        return RNVector<T, N>{v};
    }
    template <typename T, int const N>
    inline RNVector<T, N> RNVector<T, N>::operator-(RNVector<T, N> const &v2) const
    {
        T *const v = new T[N];
        for (int n = 0; n != N; ++n)
        {
            v[n] = vector_[n] - v2.vector_[n];
        }
        return RNVector<T, N>{v};
    }
    template <typename T, int const N>
    inline RNVector<T, N> RNVector<T, N>::operator*(T const s) const
    {
        T *const v = new T[N];
        for (int n = 0; n != N; ++n)
        {
            v[n] = vector_[n] * s;
        }
        return RNVector<T, N>{v};
    }
    template <typename T, int const N>
    inline T RNVector<T, N>::GetXn(int const n) const
    {
        return vector_[n];
    }
    template <typename T, int const N>
    inline int RNVector<T, N>::GetN() const
    {
        return N;
    }
    template <typename T, int const N>
    inline void RNVector<T, N>::PrintComponents() const
    {
        std::cout << '\n'
                  << "(" << '\t';
        for (int n = 0; n != N; ++n)
        {
            std::cout << vector_[n] << '\t';
        }
        std::cout << ")" << '\n';
    }
    /**
     * @brief contains information about a button's hitbox
     *
     */
    class button
    {
        sf::RectangleShape const &hitbox_;

    public:
        button(sf::RectangleShape const &hitbox) : hitbox_{hitbox} {}

        /**
         * @brief determines if button was clicked
         *
         * @param mouse vector representing mouse position
         * @return true - vector coordinates are inside the hitbox
         * @return false
         */
        bool is_pressed(sf::Vector2i const &mouse) const;
    };
} // namespace phi
#endif