/**
 * @file factorial.hxx
 * @author Federico Ciampiconi (foffy2001@duck.com)
 * @brief 
 * @version 0.1
 * @date 2025-03-07
 * 
 * @copyright Copyright (c) 2025 Federico Ciampiconi
 * Distributed under the MIT License (MIT)
 * see accompanying LICENSE.txt file
 * or a copy at https://opensource.org/licenses/MIT
 * 
 */

#ifndef FFACTORIAL_HPP
#define FFACTORIAL_HPP
#include <type_traits>
#include <limits>
/**
 * @brief Factorial of a integer, if n < 0 returns 1
 * 
 * @tparam Tint integer representation of the argument
 * @param n argument
 * @return Tint integer representation of the factorial value
 */
template <typename Tint>
inline Tint factorial(Tint n)
{
    static_assert(std::numeric_limits<Tint>::is_integer);
    Tint f = static_cast<Tint>(1);
    while (n > 1)
    {
        f *= n;
        --n;
    }
    return f;
}
/**
 * @brief Integer power of a number
 * 
 * @tparam T argument type
 * @param b base
 * @param e integer exponent
 * @return T result
 */
template <typename T>
inline T int_pow(T const b, int const e)
{
    static_assert(std::is_scalar_v<T>);
    T res = 1;
    for (int i = 0; i != e; ++i)
    {
        res *= b;
    }
    return res;
}
/**
 * @brief Binomial coefficient (n, k)
 * 
 * @tparam Tint integer type of argument
 * @param n 
 * @param k 
 * @return Tint integer type of the result 
 */
template <typename Tint>
inline Tint binomialCoefficients(Tint const n, Tint const k)
{
    static_assert(std::numeric_limits<Tint>::is_integer);
    return factorial(n) / (factorial(n - k) * factorial(k));
}
#endif
