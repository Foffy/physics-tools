/**
 * @file rt_graph.hpp
 * @author Federico Ciampiconi (foffy2001@duck.com)
 * @brief 
 * @version 0.1
 * @date 2025-03-07
 * 
 * @copyright Copyright (c) 2025 Federico Ciampiconi
 * Distributed under the MIT License (MIT)
 * see accompanying LICENSE.txt file
 * or a copy at https://opensource.org/licenses/MIT
 * 
 */

#ifndef REAL_TIME_GRAPHER_HPP
#define REAL_TIME_GRAPHER_HPP
#include "structures.hpp"
#include <vector>
#include <string>
#include <atomic>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>


namespace phi
{
    class axis
    {
        friend class Graph2D_Ft;
        sf::Font numbers_font_;
        Vector2D<float> range_;
        sf::RectangleShape axis_;
        sf::RectangleShape marker_;

        std::string axis_title_ = "title";

    public:
        axis(Vector2D<float> const &range, sf::Color const &axes_color);
        float get_max() const;
        float get_min() const;
        void set_range(Vector2D<float> const &range);
        void set_max(float const max);
        void set_min(float const min);
        void set_axis_title(std::string const &title);
        void draw_horizontal_axis(sf::RenderWindow &target_window, Vector2D<float> const &size, Vector2D<float> const &position, int const markers);
        void draw_vertical_axis(sf::RenderWindow &target_window, Vector2D<float> const &size, Vector2D<float> const &position, int const markers);
    };
    struct curve
    {
        std::string name;
        std::vector<Vector2D<float>> points;
        sf::Color curve_color;
    };
    /**
     * @brief graph of a function of time
     * 
     */
    class Graph2D_Ft
    {
        std::atomic_int total_points_ = 0;
        std::atomic_int drawn_points_ = 0;
        std::vector<curve> curves_; //vector of curves
        Vector2D<float> position_;  //top left corner position
        Vector2D<float> size_;      //X Y size vector
        axis x_;                    //X axis object
        axis y_;                    //Y axis object
        
        
    public:
        Graph2D_Ft();
        /**
     * @brief Construct a new time graph
     * 
     * @param max_t max value of time
     * @param min_t min value of time 
     * @param max_Ft max value of F(t)
     * @param min_Ft min value of F(t)
     */
        Graph2D_Ft(Vector2D<float> const &x_range, Vector2D<float> const &y_range, sf::Color const &graph_color);
        void draw_graph(sf::RenderWindow &target_window, Vector2D<float> const &pos, Vector2D<float> const &size);
        bool add_point(std::string const &name, Vector2D<float> const &point);
        void add_point(int const order_index, Vector2D<float> const &point);
        void add_curve(std::string const &name, sf::Color const &color);
        void draw_axes(sf::RenderWindow &target_window);
        void set_x_title(std::string const &title);
        void set_y_title(std::string const &title);
        void draw_leg(sf::RenderWindow &target_window);
        int get_drawn_points()const;
        int get_total_points()const;
        Vector2D<float> get_Size() const;
    };
} // namespace sir
#endif